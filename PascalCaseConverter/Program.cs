﻿using System;
using System.Globalization;

namespace PascalCaseConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Convert("The quick brown fox jumped over the lazy dog");
            Convert("The qUick bRoWn fox  jumped, OVER the  lazy.dog");
        }
        static void Convert(string phrase)
        {
            var converted = phrase.ToLower();
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            converted = info.ToTitleCase(converted).Replace(" ", "").Replace(".","").Replace("!","").Replace(",","");
            Console.WriteLine(converted);
        }
    }
}
